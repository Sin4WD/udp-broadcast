import socket
import time

sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sender.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

sender.settimeout(0.2)
message = b"hi"
while True:
    sender.sendto(message, ("<broadcast>", 5000))
    print("message sent!")
    time.sleep(1)
